package com.xxl.job.executor.service.jobhandler.batchjob;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PiLiangCustomerMapper {
    /**
     * 功能描述: 
     * <批量 customer>
     * @param piLiangCustomer
     * @date : 2019/12/6 17:16
     * @return : int
     * @author : hduong
     */
    int addCustomer(@Param("pojo") PiLiangCustomer piLiangCustomer);

    /**
     * 功能描述: 
     * <>
     * @param id
     * @date : 2019/12/6 17:36
     * @return : com.piliangzhixing.PiLiangCustomer
     * @author : hduong
     */
    PiLiangCustomer select(@Param("id") String id);

    /**
     * 功能描述: 
     * <>
     * @param piLiangCustomer
     * @date : 2019/12/6 17:36
     * @return : int
     * @author : hduong
     */
    int updateCustomer(@Param("pojo") PiLiangCustomer piLiangCustomer);
}
