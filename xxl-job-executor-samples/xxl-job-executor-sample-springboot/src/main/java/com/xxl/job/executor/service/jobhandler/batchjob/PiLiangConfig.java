package com.xxl.job.executor.service.jobhandler.batchjob;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <h3>springboot_batch_jdbc</h3>
 * <p>批量配置</p>
 *
 * @author : hduong
 * @version : 1.0
 * @date : 2019-12-06 16:59
 **/
@Configuration
public class PiLiangConfig {

    /**
     * 在被容器管理后： 直接自动注入就好了
     */
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    /**
     * 在被容器管理后： 直接自动注入就好了
     */
    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job piLiangJob() {
        return jobBuilderFactory.get("piLiangJob")
                .incrementer(new RunIdIncrementer())
                .start(piLiangStep())
                .build();
    }

    @Bean
    public Step piLiangStep() {
        return stepBuilderFactory.get("piLiangStep")
                .<PiLiangCustomer, PiLiangCustomer>chunk(10)
                .reader(commonFileReader(null))
                .processor(piLiangFileProcessor())
                .writer(piLiangFileWriter())
                .build();
    }

    @Bean
    public PiLiangFileWriter piLiangFileWriter() {
        return new PiLiangFileWriter();
    }

    @Bean
    public PiLiangFileProcessor piLiangFileProcessor() {
        return new PiLiangFileProcessor();
    }

    @Bean
    @StepScope
    public CommonFileReader commonFileReader(@Value("#{jobParameters[fileName]}") String fileName) {
        return new CommonFileReader(fileName, PiLiangCustomer.class, ",",null);
    }

}
