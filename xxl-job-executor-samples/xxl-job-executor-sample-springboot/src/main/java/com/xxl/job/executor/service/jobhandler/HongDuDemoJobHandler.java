package com.xxl.job.executor.service.jobhandler;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * <h3>xxl-job</h3>
 * <p>任务自定义</p>
 * spring batch 定义job
 * 这里启动job
 * @author : hduong
 * @version : 1.0
 * @date : 2019-12-09 13:17
 **/
@Component
@JobHandler(value = "hongDuJobHandler")
public class HongDuDemoJobHandler extends IJobHandler {


    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier("piLiangJob")
    private Job job;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        String fileName = "customer.txt";
        JobParameters jobParameters = new JobParametersBuilder()
                .addDate("date", new Date())
                .addString("fileName", fileName)
                .toJobParameters();
        JobExecution jobExecution = null;
        try {
            jobExecution = jobLauncher.run(job, jobParameters);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("执行任务失败...............");
        }
        return new ReturnT<>(jobExecution.getExitStatus().getExitDescription());
    }
}
