package com.xxl.job.executor.service.jobhandler.batchjob;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <h3>springboot_batch_jdbc</h3>
 * <p>批量写入</p>
 *
 * @author : hduong
 * @version : 1.0
 * @date : 2019-12-06 17:24
 **/
public class PiLiangFileWriter implements ItemWriter<PiLiangCustomer> {

    @Autowired
    private PiLiangCustomerMapper piLiangCustomerMapper;

    private volatile int result;

    @Override
    public void write(List<? extends PiLiangCustomer> list) throws Exception {
        for (PiLiangCustomer piLiangCustomer : list) {
            PiLiangCustomer temp = piLiangCustomerMapper.select(piLiangCustomer.getId());
            if(temp != null) {
                result += piLiangCustomerMapper.updateCustomer(piLiangCustomer);
            } else {
                result += piLiangCustomerMapper.addCustomer((PiLiangCustomer)piLiangCustomer);
            }
        }
        System.out.println("执行成功数： " + result);
    }
}
