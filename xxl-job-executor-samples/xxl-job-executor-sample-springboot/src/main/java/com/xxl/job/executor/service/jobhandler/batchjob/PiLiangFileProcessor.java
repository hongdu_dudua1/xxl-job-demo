package com.xxl.job.executor.service.jobhandler.batchjob;

import org.springframework.batch.item.ItemProcessor;

/**
 * <h3>springboot_batch_jdbc</h3>
 * <p>批量文件处理器</p>
 *
 * @author : hduong
 * @version : 1.0
 * @date : 2019-12-06 17:23
 **/
public class PiLiangFileProcessor implements ItemProcessor<PiLiangCustomer, PiLiangCustomer> {

    @Override
    public PiLiangCustomer process(PiLiangCustomer piLiangCustomer) throws Exception {
        return piLiangCustomer;
    }
}
