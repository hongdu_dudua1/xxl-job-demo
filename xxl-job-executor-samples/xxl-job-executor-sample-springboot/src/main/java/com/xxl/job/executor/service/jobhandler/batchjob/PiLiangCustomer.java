package com.xxl.job.executor.service.jobhandler.batchjob;

/**
 * <h3>springboot_batch_jdbc</h3>
 * <p>批量customer</p>
 *
 * @author : hduong
 * @version : 1.0
 * @date : 2019-12-06 17:15
 **/
public class PiLiangCustomer {
    private String id;
    private String firstName;
    private String lastName;
    private String birthday;

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday='" + birthday + '\'' +
                '}';
    }

    public PiLiangCustomer() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
}
