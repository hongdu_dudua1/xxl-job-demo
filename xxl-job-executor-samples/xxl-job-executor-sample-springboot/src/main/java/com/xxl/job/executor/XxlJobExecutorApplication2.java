package com.xxl.job.executor;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xuxueli 2018-10-28 00:38:13
 */
@SpringBootApplication
@EnableBatchProcessing(modular = true)
public class XxlJobExecutorApplication2 {

	public static void main(String[] args) {
        SpringApplication.run(XxlJobExecutorApplication2.class, args);
	}

}